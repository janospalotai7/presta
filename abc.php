<?php
if (!defined('_PS_VERSION_'))
    exit;

require (dirname(__FILE__).'/adatok.class.php');

class Abc extends Module
{

    private $_menu = '';
    private $_html = '';
    public function __construct()
    {

        $this->name = 'abc';
        $this->tab = 'Abc';
        $this->version = 1.0;
        $this->author = 'Palotai Janos';
        $this->need_instance = 0;


        parent::__construct();

        $this->displayName = $this->l('Abc');
        $this->description = $this->l('ABC');
    }

    public function install()
    {
        if (parent::install() == false OR !$this->registerHook('leftColumn') OR !$this->installDB() OR !$this->registerHook('displayTop'))
            return false;
        return true;
    }

    public function installDb()
    {
        return (Db::getInstance()->execute('
			 CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'abc_menu` (
			`id` INT(11) UNSIGNED NOT NULL,
			`label` VARCHAR( 128 ) NOT NULL ,
			`link` VARCHAR( 128 ) NOT NULL ,
			`table` VARCHAR( 128 ) NOT NULL ,
			`field` VARCHAR( 128 ) NOT NULL ,
			`abc_e` TINYINT( 1 ) NOT NULL,
			`new_e` TINYINT( 1 ) NOT NULL,
			INDEX (`id`)
		) ENGINE = '._MYSQL_ENGINE_.' CHARACTER SET utf8 COLLATE utf8_general_ci;'));

    }

    public function uninstall()
    {
        if (!parent::uninstall())
            Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'abc_menu`');
        parent::uninstall();
        !$this->uninstallDB();
    }

    private function uninstallDb()
    {
        Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'abc_menu`');
        return true;
    }

    private function makeMenu()
    {

        /*
         * Országok ABC rendbe rendezve, vízszintes menüben
         * */
        $this->_menu .= '<script src="http://code.jquery.com/jquery-latest.js"></script>';
        $this->_menu .= '<script src="http://code.jquery.com/jquery-latest.min.js"></script>';
        $this->_menu .= '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>';
        $this->_menu .= '<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>';
        $this->_menu .= '<script type="text/javascript">';
        $this->_menu .= '$(document).ready(function(){';
        $this->_menu .= '$("a.abc_menu").click(function(){';
        $this->_menu .= 'if($(this).parent().next("#abc").is(":hidden")){ ';
        $this->_menu .= '$(this).parent().next("#abc").show("fast");';
        $this->_menu .= '} ';
        $this->_menu .= 'else{';
        $this->_menu .= '$(this).parent().next("#abc").hide("fast");';
        $this->_menu .= '}';
        $this->_menu .= '});';
        $this->_menu .= '$("a.abc").click(function(){';
        $this->_menu .= 'if($(this).next(".betuk").is(":hidden")){ ';
        $this->_menu .= '$(".betuk").hide();';
        $this->_menu .= '$(this).next(".betuk").show("fast");';
        $this->_menu .= '} ';
        $this->_menu .= 'else{';
        $this->_menu .= '$(this).next(".betuk").hide("fast");';
        $this->_menu .= '$(".betuk").hide();';
        $this->_menu .= '}';
        $this->_menu .= '});';
        $this->_menu .= '});';
        $this->_menu .= '</script>';
        $this->_menu .= '<style>a.abc,a.abcde{';
        $this->_menu .= 'background: #000;display: inline;border-right: 1px solid gray;margin:0;float: left;border-bottom: 1px solid gray;width: 20px;';
        $this->_menu .= '}';
        $this->_menu .= 'a.abcde{background: #666;width: 200px;}';
        $this->_menu .= '.betuk{text-align: center;display:none;position:absolute;overflow:hidden;margin: 70px auto;}';
        $this->_menu .= '</style>';


        $rows=Adatok::getA();
        $this->_menu .= '<li><a class="abc_menu">'.$rows[0]["label"].'</a></li>';

        $this->_menu .= '<div id="abc" style="text-align: center;display:none;position:absolute;margin-top: 34px;min-width: 100px;border-top: 1px solid gray;">';




        $sql = 'SELECT distinct '.$rows[0]["field"].' FROM '._DB_PREFIX_.$rows[0]["table"].' order by '.$rows[0]["field"].' asc';
        $arr = array();
        if ($results = Db::getInstance()->ExecuteS($sql))
            foreach ($results as $row)
            {
                $kar=substr($row["name"], 0, 1);
                if(in_array($kar,$arr))
                    $uj=false;
                else
                {
                    $arr[]=$kar;
                    $uj=true;
                }
                if($uj)
                {
                    $this->_menu .= '<a class="abc">'.$kar.'</a>';

                    $sql2 = 'SELECT distinct '.$rows[0]["field"].' FROM '._DB_PREFIX_.$rows[0]["table"].' where '.$rows[0]["field"].' like "'.$kar.'%" order by '.$rows[0]["field"].' asc';
                    if ($results2 = Db::getInstance()->ExecuteS($sql2))
                    {
                        $this->_menu .= '<div class="betuk">';
                        foreach ($results2 as $rows2)
                        {
                            $this->_menu .= '<a class="abcde">'.$rows2["name"].'</a>';
                        }
                        $this->_menu .= '</div>';
                    }
                }
            }

        $this->_menu .= '</div>';
        /*
         * VÉGE
         * */
    }


    public function getContent()
    {
        if (isset($_POST["m"]))
        {
            if(Adatok::add(Tools::getValue('label'),Tools::getValue('link'),Tools::getValue('table'),Tools::getValue('field'),Tools::getValue('abc_e'),Tools::getValue('new_e')))
            $this->_html .= $this->displayConfirmation($this->l('The link has been added.'));
        }



        $this->_html .= '
		<fieldset>
			<legend><img src="../img/admin/add.gif" alt="" title="" />'.$this->l('Add Abc Menu Top Link').'</legend>
			<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" id="form">';

            $this->_html .= '
					<div id="link_label" style="display: block">
				<label>'.$this->l('Label').'</label>
				<div class="margin-form">
						<input type="text" name="label" id="label" size="70"/>
			  </div>
					';

            $this->_html .= '
				  <label>'.$this->l('Link').'</label>
				<div class="margin-form">
					<input type="text" name="link" id="link" size="70" />

				</div>';
            $this->_html .= '<label style="clear: both;">'.$this->l('Abc menu?').'</label>
				<div class="margin-form">
					<input style="clear: both;" type="checkbox" name="abc_e" value="1" '.(isset($abc_e) && $abc_e ? 'checked' : '').'/>
				</div>';

            $this->_html .= '
				  <label>'.$this->l('Tábla').'</label>
				<div class="margin-form">
					<input type="text" name="table" id="table" size="70" />

				</div>';

            $this->_html .= '
				  <label>'.$this->l('Mező').'</label>
				<div class="margin-form">
					<input type="text" name="field" id="field" size="70" />
				</div>
				</div>';

            $this->_html .= '<label style="clear: both;">'.$this->l('New Window').'</label>
				<div class="margin-form">
					<input style="clear: both;" type="checkbox" name="new_e" value="1" '.(isset($new_window_edit) && $new_window_edit ? 'checked' : '').'/>
				</div>
<div class="margin-form">';


        $this->_html .= '
					<input type="hidden" name="m" value="m" class="button" />
					<input type="submit" name="submitBlocktopmenuLinks" value="'.$this->l('Add	').'" class="button" />
</div>

			</form>
		</fieldset><br />';
        /*
        $this->_html .= '
		<fieldset>
			<legend><img src="../img/admin/details.gif" alt="" title="" />'.$this->l('List Menu Top Link').'</legend>
			<table style="width:100%;">
				<thead>
					<tr style="text-align: left;">
						<th>'.$this->l('Id Link').'</th>
						<th>'.$this->l('Label').'</th>
						<th>'.$this->l('Link').'</th>
						<th>'.$this->l('Table').'</th>
						<th>'.$this->l('Field').'</th>
						<th>'.$this->l('Abc').'</th>
						<th>'.$this->l('New Window').'</th>
						<th>'.$this->l('Action').'</th>
					</tr>
				</thead>
				<tbody>';
        foreach ($links as $link)
        {
            $this->_html .= '
					<tr>
						<td>'.(int)$link['id_linksmenutop'].'</td>
						<td>'.Tools::safeOutput($link['label']).'</td>
						<td><a href="'.Tools::safeOutput($link['link']).'"'.(($link['new_window']) ? ' target="_blank"' : '').'>'.Tools::safeOutput($link['link']).'</a></td>
						<td>'.Tools::safeOutput($link['table']).'</td>
						<td>'.Tools::safeOutput($link['field']).'</td>
						<td>'.(($link['abc_e']) ? $this->l('Yes') : $this->l('No')).'</td>
						<td>'.(($link['new_window']) ? $this->l('Yes') : $this->l('No')).'</td>
						<td>
							<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
								<input type="hidden" name="id_linksmenutop" value="'.(int)$link['id_linksmenutop'].'" />
								<input type="submit" name="submitBlocktopmenuEdit" value="'.$this->l('Edit').'" class="button" />
								<input type="submit" name="submitBlocktopmenuRemove" value="'.$this->l('Remove').'" class="button" />
							</form>
						</td>
					</tr>';
        }
        $this->_html .= '</tbody>
			</table>
		</fieldset>';
        */

        return $this->_html;
    }

    public function hookDisplayTop( $params )
    {
        global $smarty;

        $this->makeMenu();
        $this->smarty->assign('MENU', $this->_menu);

        $html = $this->display(__FILE__,'abc.tpl');

        return $html;
    }
}

?>