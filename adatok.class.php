<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Adatok
{
    public static function add($label, $link, $table, $field,$abc_e,$new_e)
	{
        $id = Db::getInstance()->Insert_ID();
        if
        (Db::getInstance()->insert('ps_abc_menu', array(
            'id' => (int)$id,
            'label'    => pSQL($label),
            'link'    => pSQL($link),
            'table'    => pSQL($table),
            'field'    => pSQL($field),
            'abc_e'    => (int)$abc_e,
            'new_e'    => (int)$new_e
        )))
            return true;
        else
            return false;

	}

    public static function getA()
    {
        $sql = 'SELECT *
				FROM '._DB_PREFIX_.'abc_menu';

        return Db::getInstance()->executeS($sql);
    }
}

?>
